#!/usr/bin/python
#!/usr/bin/python
# coding=utf-8
import cgi,cgitb

class View:
	def __init__(self, body="", error=""):
		self.body	= body
		self.error = error
		self.default = """
		<form  id="addresses" action="handler.py" autocomplete="on" method="POST" enctype="multipart/form-data"> 
                                <h1>Meilleur trajet</h1> 
                                %s
                                <fieldset> 
                                	<legend>Charger le fichier d'adresses</legend>                         
                                    <label for="addfile" > Fichier *</label>
                                    <input name="addfile" required="required" type="file" /> 
                                </fieldset>
                                
                                <p id="buttons" class="login button">                                     
                                    <input id="submit" type="submit" value="Charger" /> 
								</p>

                            </form>
		"""
		
	
	def get_view(self):
		output = """
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
        <title>Plus Court Trajet</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
        <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="../css/demo.css" />
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
		<link rel="stylesheet" type="text/css" href="../css/animate-custom.css" />
    </head>
    <body>
    	<script>
    	<!--
    	var num_addr = 1;
    	function addField()
    	{
    		var elform = document.getElementById("addresses");
    		var button = document.getElementById("buttons");
    		
    		var new_fset = document.createElement("fieldset");
    		
    		var contain = '<legend>Place'+num_addr+'</legend><label for="address" > Adresse: </label><input name="address'+num_addr+'" required="required" type="text" placeholder="exemple: 185 avenue Albert Thomas, 87065 Limoges"/ ><label for="nom" > Nom: </label><input type="text" name="name'+num_addr+'" placeholder="exemple: chez Microspot" required="required"/ >';
    		new_fset.innerHTML = contain;

			num_addr++;
			
    		elform.insertBefore(new_fset, button);
    		elForm.insertBefore(document.createElement("br"), button);
    	}
    	-->
    	</script>
        <div class="container">
            <!-- Codrops top bar -->
            <div class="codrops-top">
                <a href="/">
                    <strong>&laquo; Page principale&raquo </strong><!--Responsive Content Navigator-->
                </a>
               <!-- <span class="right">
                    <a href=" http://tympanus.net/codrops/2012/03/27/login-and-registration-form-with-html5-and-css3/">
                        <strong>Back to the Codrops Article</strong>
                    </a>
                </span>
               !-->
                <div class="clr"></div>
            </div><!--/ Codrops top bar -->
            <header>
                <!--<h1>Login and Registration Form <span>with HTML5 and CSS3</span></h1>-->
            </header>
            <section>				
                <div id="container_demo" >
                    <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->

                    <div id="wrapper">
                        <div id="login" class="animate form">                        	
                            %s
                        </div>
						
                    </div>
                </div>  
            </section>
        </div>
    </body>
</html>
"""
		if not self.body:
			return output%(self.default%(self.error))
		return output%(self.body)
