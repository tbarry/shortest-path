#!/usr/bin/python
# coding=utf-8
import sys, cgi,cgitb, time
import view
from shortest_path import *
from core import Main

cgitb.enable()			#pour afficher les erreurs de façon claire
fileContent = {}
myview = view.View()	#initialisation de la vue
error = ""

print 'Content-Type: text/html'
print

def set_error(msg):
	return "<div id= 'error'>%s</div>"%msg

def log(msg):
	sys.stderr.write(msg)

#fonction qui lit les adresses contenues dans le fichier uploadé
def load_addresses():	
	global error
	form = cgi.FieldStorage()
	if not form.has_key("addfile"):
		return False
	_file = form["addfile"].file
	log("loading addresses .... ");t=time.time();
	line = ""
	i = 1
	while True:
		line = _file.readline()
		if not line:break;
		if not line.rstrip():continue
		add = filter(lambda l:l, [x.strip() for x in line.split("\t")])
		if len(add)!=2:
			error =  set_error("Format d'adresse incorrecte: <u><i>%s</i></u> (ligne %d du fichier)</div>"%(line,i))
			log("[FAIL]\n");
			return False
		fileContent[add[0]] = add[1]
		i+=1
	log("[OK] %s\n"%(time.time()-t));
	return True


#recupération des adresses
if not load_addresses():
	myview.error = error
	print myview.get_view()
	error = "";
	sys.exit(0)

#vérification qu'une position de départ est indiquée
if "position" not in fileContent.keys():
	myview.error = set_error("Une adresse de départ nommée <u>position</u> doit figurer dans la liste")
	print myview.get_view()
	error = "";
	sys.exit(0)
	
#si le nombre d'adresse est < 4: pas la peine d'utiliser ce programme
if len(fileContent.keys())<4:
	myview.error = set_error("Pas assez d'adresses, alors je garde ma RAGE")
	print myview.get_view()
	sys.exit(0)

#création des objet "Address"
#puis vérification de la validité des adresses
i = 1
addr = []
log("creating address Objects .... ");t=time.time();
error = set_error("L'adresse: %s est incorrecte")
for key in fileContent.keys():
	add = Address(i, fileContent[key], key)
	if not add.gCode():
		myview.error = error%fileContent[key]
		myview.body  = ""
		print myview.get_view()
		log("[FAIL]\n");
		sys.exit(0)
	addr.append(add)
	i+=1
log("[OK] %s\n"%(time.time()-t))

#calcul des distances entre toutes les places
log("computing distances ... ");t=time.time();
dist = Distance(addr)
dist.compute_dist()
log("[OK] %s\n"%(time.time()-t));

#préparation des données pour le module "core"
log("preparing data for core module ...");t=time.time();
places 		= [add.id for add in addr]
#start_place = [add.id for add in addr if add.name=="position"][0]
start_place = dist.position.id
dvector		= dist.dvector
log("[OK] %s\n"%(time.time()-t));
log("starting shortest path algo ...");t=time.time();
main = Main.Main(
			places  	= places,
			dvector 	= dvector,
			nb_tour 	= 15,
			start_place	= start_place,
			logging		= False)
main.run()
log("[OK] %s\n"%(time.time()-t));

best_path = [addr[i-1] for i in main.best_path] #liste des "adresse" du meilleur trajet "ordered"

body = """
<h1> Meilleur trajet obtenu </h1>
<div align='center'>
<h2>distance total a parcourir:<strong> %s km</strong></h2>
<addr style="font-size:14px">cette distance tient compte du trajet de retour à la position de départ.</addr>
<table >
	<tr>
		<td>
"""%main.best_value
for i in range(len(best_path)):
	body+="<div id='letter'> %s  &nbsp;<strong> %s </strong></div><addr>%s</addr><hr/>"% (chr(65+i), best_path[i].name, best_path[i].formatted_addr)
body+="</td>"

pos	= best_path[0]

#formulation de la requete vers l'API google map
#pour l'affichage de la carte avec des marqueurs et des chemins
req	= "http://maps.googleapis.com/maps/api/staticmap?center=%s&size=600x400"
req	= req%"%s,%s"%(pos.gcode['lat'],pos.gcode['lng'])

markers	= "&markers=color:blue|label:%s|%s,%s"
path = "&path=color:0x0000ff|weight:5"
to_mark = ""
for i in range(len(best_path)-1):
	tmp = markers
	tmp = tmp%(chr(65+i), best_path[i].gcode['lat'], best_path[i].gcode['lng'])
	path += "|%s,%s"%(best_path[i].gcode['lat'], best_path[i].gcode['lng'])
	to_mark+=tmp
path += "|%s,%s"%(pos.gcode['lat'],pos.gcode['lng'])
req = req+to_mark+path+"&sensor=false"

body+="<td><img src='%s' alt='Image Google Map' /></td></tr></table>"%req

tabdist = "<br/><h2>Table des distances entre toutes les adresses</h2><table width='100%'><tr><th scope='col'>  </th>"
for add in addr: tabdist+=" <th scope='col'><strong>%s</strong></th> "%add.name
tabdist+="</tr bgcolor='blue'>"
for add1 in addr:
	tabdist+="<tr><th scope='row'><strong>%s</strong></th>"%add1.name
	for add2 in addr:
		tabdist+="<td bgcolor='#ccc'> %5s km<br/>&nbsp;</td>"%dist.get_dist_btw(add1, add2)
	tabdist+="</tr>"
tabdist+="</table>"

body+=tabdist		
	
myview.body = body
print myview.get_view()







