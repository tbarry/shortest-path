#!/usr/bin/python
# -*- coding: utf8 -*-

import random
class Ant:
	def __init__(self, graph, start, places, dist_min):
		self.graph 		= graph
		self.path 		= [start]
		self.start 		= start
		self.dist_min 	= dist_min
		self.places 	= places
		self.nb_places 	= len(places)
		self.value 		= 0		
		return
	
	#evalue le chemin emprunté
	#puis depose des pheromone tout au long du chemin
	#et ensuite marquer l'arete comme étant "crossed" qui sert pour 
	#pour l'évaporation des pheros
	def evaluate(self):
		for i in range(1, len(self.path)):
			self.value += self.graph.get_arete(self.path[i-1], self.path[i]).dist
		self.phero = 100 * (self.dist_min * self.nb_places)/self.value
		for i in range(1, len(self.path)):
			self.graph.get_arete(self.path[i-1], self.path[i]).phero = self.phero
			self.graph.get_arete(self.path[i-1], self.path[i]).crossed = True

	#phase d'éclairage ou d'initialisation:
	#chaque fourmi éffectue une circuit au hasard puis l'évalue
	def first_tour(self):
		for i in range(self.nb_places-1):
			nxt = self.places[random.randint(0,self.nb_places-1)]
			while nxt in self.path:
				nxt = self.places[random.randint(0,self.nb_places-1)]
			self.path.append(nxt)
		self.path.append(self.start)
		self.evaluate()
	
	def make_tour(self):
		self.erase_mem()
		for i in range(self.nb_places):
			self.next()
		self.evaluate()
		return
	
	#effacer la mémoir de la fourmi
	def erase_mem(self):
		self.path = [self.start]
		self.value = 0
	
	#parmi tous les voisins du point courant,
	#si une des arete n'a pas encore le phero calculé(non traversée):
	#on va vers un voisin pris au hasard
	#sinon, on va vers celui qui a le plus grand phero
	def next(self):
		if len(self.path) == self.nb_places:
			self.path.append(self.start)
			return
		neighb = []
		guide_by_phero = True
		for place in self.places:
			if place not in self.path:
				neighb.append(place)
				if self.graph.get_arete(self.path[len(self.path)-1], place).phero == 0:
					guide_by_phero = False
		nxt_place = self.get_nxt_place(neighb, guide_by_phero)
		self.path.append(nxt_place)
		return


	
	#fonction auxiliaire à la fonction "next(self)"
	def get_nxt_place(self, neighb, guide_by_phero):
		curr = self.path[len(self.path)-1]
		nxt = neighb[0]
		if not guide_by_phero:
			nxt = neighb[random.randint(0, len(neighb)-1)]
		else:
			best_phero = self.graph.get_arete(curr, nxt).phero
			for place in neighb:
				arete = self.graph.get_arete(curr, place)
				if arete.phero > best_phero:
					best_phero = arete.phero
					nxt = place
		return nxt

			
		
	
	def __eq__(self, other_ant):
		if len(self.path) != len(other_ant.path):
			return False		
		for i in range(len(self.path)):
			if self.path[i] != other_ant.path[i]:
				return False
		return True
	
	
	def __repr__(self):
		ret = ""
		for place in self.path:
			ret+= "%s "%place
		ret+= " --> %s"%self.value
		return ret
	
	
