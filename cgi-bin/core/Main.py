#!/usr/bin/python

import math, random
import graph, ant
import sys

class Main:
	def __init__(self, places=[], dvector=[], nb_tour=5, start_place=1, logging=False):
		self.places 		= places
		self.start_place 	= start_place
		self.dvector 		= dvector
		sys.stderr.write("-creating graph ...")
		self.graph 			= graph.Graph(self.dvector, evap_coef=10)
		sys.stderr.write("[OK]")
		self.nb_tour 		= nb_tour
		self.dmin 			= min(self.dvector, key = lambda x: x[2])[2]
		self.ants			= []
		#self.nb_ants 		= math.factorial(len(self.places)-1)
		self.nb_ants		= (len(self.places)-1)*100
		self.best_path		= None
		self.best_value		= None
		self.logging		=logging
		sys.stderr.write("-creating ants ...")
		for i in range(self.nb_ants):
			self.ants.append(ant.Ant(self.graph, self.start_place, self.places, self.dmin))
		sys.stderr.write("[OK]")
	
	def log(self, msg):
		if self.logging:
			sys.stderr.write(msg)
	def run(self):
		self.log( "start place: %s"%self.start_place)
		#chaque fourmi fait un tour au hasard
		sys.stderr.write("-first tour ...")
		for ant in self.ants:
			ant.first_tour()
		self.select_best_path()
		sys.stderr.write("[OK]")
		
		self.log("\tfirst tour")
		self.log("graph")
		self.log(self.graph)
		self.log("ants")
		self.view_ants()
		
		#chaque fourmi effecture "nb_tour-1" tours
		sys.stderr.write("-making tours ...")
		for i in range(1, self.nb_tour+1):
			for ant in self.ants:
				ant.make_tour()			
			self.select_best_path()
			self.log("\n\ttour %s"%i)
			self.log("graph")
			self.log(self.graph)
			self.log("ants")
			self.view_ants()
			self.graph.phero_evaporate()
		sys.stderr.write("[OK]")
		#print "\nBest path: %s --> %s"%(self.best_path, self.best_value)

		
		
	def select_best_path(self):
		if not self.best_path:
			self.best_path = self.ants[0].path
			self.best_value = self.ants[0].value
		for ant in self.ants:
			if ant.value < self.best_value:
				self.best_value = ant.value
				self.best_path = ant.path
		return
	
	def view_ants(self):
		for ant in self.ants:
			self.log(ant)
		return
		


if __name__ == "__main__":

	places = [1,2,3,4,5]
	dvector = []
	for i in range(len(places)-1):
		for j in range(i+1,len(places)):
			dvector.append((places[i],places[j], float(random.randint(10,100))))
	print dvector
	print len(dvector)
	main = Main(places  	= places, 
				dvector 	= dvector,
				nb_tour 	= 5,
				start_place	= 1,
				logging		= False)
	main.run()
	print "\nBest path: %s --> %s"%(main.best_path, main.best_value)

