#!/usr/bin/python
# -*- coding: utf8 -*-
import arete

class Graph:
	def __init__(self, dvector=[], evap_coef=5):
		self.dvector 	= dvector
		self.graph 		= {}
		self.evap_coef 	= evap_coef
		self.init()
	
	#remplissage du graph a partir du vecteur de couple (place, place, dist)
	def init(self):
		for elm in self.dvector: #elm = (v1,v2,dist)
			if self.graph.has_key(elm[0]):
				self.graph[elm[0]][elm[1]] = arete.Arete(elm[2], 0)
			elif self.graph.has_key(elm[1]):
				self.graph[elm[1]][elm[0]] = arete.Arete(elm[2], 0)
			else:
				self.graph[elm[0]] = {}
				self.graph[elm[0]][elm[1]] = arete.Arete(elm[2], 0)
	
	
	def get_arete(self,place1, place2):	
		arete = None
		try:
			arete = self.graph[place1][place2]
		except:
			try:
				arete = self.graph[place2][place1]
			except:
				return None
		return arete
	
	def phero_evaporate(self):
		for key in self.graph.keys():
			for kkey in self.graph[key].keys():
				if self.graph[key][kkey].crossed:
					self.graph[key][kkey].crossed = False
				else:
					self.graph[key][kkey].decr_phero(self.evap_coef)
			
			
			
	def __repr__(self):
		ret = ""
		for key in self.graph.keys():
			for kkey in self.graph[key].keys():
				ret += "%s <-> %s : %s\n"%(key, kkey, self.graph[key][kkey])
		return ret
			
		


if __name__ == "__main__":
	dvector = [('1','2',50.0), ('1','3',60.0), ('1','4',55.0),('2','3',50.0), ('4','2',500.0)]
	graph = Graph(dvector)
	print graph
	
