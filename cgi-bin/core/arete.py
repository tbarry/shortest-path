#!/usr/bin/python
# -*- coding: utf8 -*-

class Arete:
	
	def __init__(self, dist, phero):
		self.dist 		= dist
		self.phero 		= phero
		self.crossed 	= False
	
	@property
	def phero(self):
		return self.phero

	@phero.setter
	def phero(self, phero):
		self.phero = max(self.phero, phero)
	
	def decr_phero(self, coef):
		if not self.phero:
			return
		self.phero = max(0, self.phero/coef)
        
	def __repr__(self):
		return "(%s,%s,%s)"%(self.dist, self.phero,self.crossed)
