#!/usr/bin/python
# -*- coding: utf8 -*-

from random import randint
from urllib2 import urlopen
import simplejson

class Address:
	def __init__(self, id, addr, name):
		self.id				= id
		self.addr			= addr
		self.name			= name
		#tr_addr:transformed addr e.g: "Limoges, France" --> "Limoges+France"
		self.tr_addr		= '+'.join(addr.split())
		self.gcode = {}
		self.formatted_addr	= None
	
	#permet d'obténir les coordonnées géographique d'une place
	#latitude et longitude
	#on s'en sert aussi pour vérifier qu'une place est valide
	#en retournant True ou False
	def gCode(self):
		url = "http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=true"%self.tr_addr
		req = urlopen(url)
		res = simplejson.load(req)
		if res['status'] != 'OK':
			return False
		self.gcode = res['results'][0]['geometry']['location']
		self.formatted_addr = (res['results'][0]['formatted_address']).encode('utf-8')
		return True

	def __eq__(self, addr):
		return self.id == addr.id and self.addr == addr.addr \
				and self.name == addr.name
	
	def __repr__(self):
		ret =  """
		*Address*
			id              : %s
			name            : %s
			addr            : %s
			formatted_addr  : %s
			location        : %s
			"""%(self.id, self.name, self.addr,self.formatted_addr,self.gcode)
		return ret



class Distance:
	def __init__(self,addr=[], mode="driving", sensor="false"):
		self.addr			= addr		#une liste d'objet de type "Address"
		self.mode			= mode
		self.sensor			= sensor		
		self.dvector 		= []	#vecteur de distance (add1.id, add2.id, dist(add1,add2))
		self.dico_dist		= {}	#pareil que dvector, mais en mode dico
		#extraire le point de depart du parcours qui correspond à la place de nom "position"
		self.position = [add for add in self.addr if add.name=="position"][0]
	
	#calcul des distances entre toutes les place à visiter
	#return: True ou False selon le resultat de l'opération
	def compute_dist(self):
		base_url = "http://maps.googleapis.com/maps/api/distancematrix/json?"
		base_url += "origins=%s&destinations=%s&mode=%s&language=fr-FR&sensor=%s"
		for i in range(len(self.addr)-1):
			for j in range(i+1,len(self.addr)):
				url = base_url%(self.addr[i].tr_addr, self.addr[j].tr_addr, self.mode, self.sensor)
				req = urlopen(url)
				res = simplejson.load(req)
				if res['rows'][0]['elements'][0]['status'] != 'OK':
					return False
				#extraction de la distance en "km"
				##dist = float(res['rows'][0]['elements'][0]['distance']['text'][:-2].replace(",",".")) 	# {eg: text:234 km}
				dist = int(res['rows'][0]['elements'][0]['distance']['value'])
				dist = (((dist << 1)//1000)+1)>>1 # {eg: text:234 km} (((n << 1) // d) + 1) >> 1
				self.dvector.append((self.addr[i].id, self.addr[j].id, dist))
				if not self.dico_dist.has_key(self.addr[i].id):
					self.dico_dist[self.addr[i].id] = {}
				self.dico_dist[self.addr[i].id][self.addr[j].id] = dist
		return True
	
	def get_dist_btw(self, add1, add2):
		dist = 0
		try:
			dist = self.dico_dist[add1.id][add2.id]
		except:
			try:
				dist = self.dico_dist[add2.id][add1.id]
			except:
				return 0
		return dist
	
	def __repr__(self):
		ret = '\tAddress:\n'
		for add in self.addr:
			ret+=repr(add)
		ret += "\n\tdist Vector\n"
		for dvect in self.dvector:
			ret+=repr(dvect)
		return ret

		
if __name__ == "__main__":	
	add1 = Address(1, "paris", "chez moi")
	add2 = Address(2, "3 allée des forsythias, 33600 Pessac", "position")
	add3 = Address(3, "avenue prevost, 33400 Talence", "village1")
	addr = [add1,add2,add3]
	if(add1.gCode()):print "address %s: OK"%add1.id
	if(add2.gCode()):print "address %s: OK"%add2.id
	if(add3.gCode()):print "address %s: OK"%add3.id
	dist = Distance(addr)
	dist.compute_dist()
	print dist
	#print add1,add2
	


